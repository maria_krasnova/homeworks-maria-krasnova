import java.util.Scanner;

public class vocabulary {
    public static void sortBySelection(char[][] array) {
        int indexOfMin;

        for (int i = 0; i < array.length; i++) {
            char[] min = array[i];
            indexOfMin = i;

            for (int j = i + 1; j < array.length; j++) {
                if (stringsCompare(array[j], min) == -1) {
                    min = array[j];
                    indexOfMin = j;
                }
            }

            char[] temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;
        }
    }


    public static int toInt(char text[]) {

        int numder = 0, zero = '0';

        for (int i = 0; i < text.length; i++) {
            numder *= 10;
            numder += (text[i] - zero);
        }

        return numder;
    }

    public static int stringsCompare(char a[], char b[]) {

        char[] boolA = new char[Math.min(a.length, b.length)];
        char[] boolB = new char[Math.min(a.length, b.length)];

        for (int i = 0; i < Math.min(a.length, b.length); i++) {
            if (a[i] < b[i]) {

                boolA[i] = '1';
                boolB[i] = '2';

            } else if (a[i] > b[i]) {

                boolA[i] = '2';
                boolB[i] = '1';

            } else {

                boolA[i] = '1';
                boolB[i] = '1';

            }

        }

        int flag = 0;
        int numderA = toInt(boolA), numberB = toInt(boolB);


        if (numderA == numberB) {

            if (a.length < b.length) flag = -1;
            else if (a.length == b.length) flag = 0;
            else flag = 1;

        } else if (numderA < numberB) {

            if (a.length <= b.length) flag = -1;
            else if (a.length > b.length) flag = 1;

        } else if (numderA > numberB) {
            flag = 1;
        }

        return flag;

    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char lines[][] = new char[4][];
        for (int i = 0; i < lines.length; i++) {
            lines[i] = scanner.nextLine().toCharArray();
        }

        sortBySelection(lines);

        for (int i = 0; i < lines.length; i++) {
            for (int j = 0; j < lines[i].length; j++) {
                System.out.print(lines[i][j] + " ");
            }
            System.out.println();
        }
    }
}
