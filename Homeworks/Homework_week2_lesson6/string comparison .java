import java.util.Scanner;

class Program{

		public static int toInt(char text[]){

		int numder=0,zero='0';

		for(int i=0; i<text.length; i++)
		{
			numder*=10;
			numder+=(text[i]-zero);
		}

		return numder;
	}

	public static int stringsCompare(char a[],char b[]){

		char[] boolA=new char[Math.min(a.length,b.length)];
		char[] boolB=new char[Math.min(a.length,b.length)];

		for(int i=0; i<Math.min(a.length,b.length); i++)
		{
			if (a[i]<b[i]) { 

				boolA[i]='1';
				boolB[i]='2';

			} else if (a[i]>b[i]) {

				boolA[i]='2';
				boolB[i]='1';

			} else {

				boolA[i]='1';
				boolB[i]='1';

			}
			
		}

		int flag=0;
		int numderA=toInt(boolA),numberB=toInt(boolB);


		if (numderA==numberB) {

			if (a.length<b.length) flag=-1;
			else if (a.length==b.length) flag=0;
			else flag= 1;

		} else if(numderA<numberB){

			if (a.length<=b.length) flag= -1;
			else if(a.length>b.length) flag=1;

		}else if (numderA>numberB) {
			flag=1;
		}

		return flag;

	}

	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);

		char a[]=scanner.nextLine().toCharArray();
		char b[]=scanner.nextLine().toCharArray();

		System.out.print(stringsCompare(a,b));
	}

}