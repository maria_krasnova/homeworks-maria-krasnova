import java.util.Scanner;

class Program{

	public static char theMostCommonLetters(char text[])
	{
		int count;
		int maxNumberOfMeetings = 0;
		char maxChar='0';
		for (char i = 'A'; i < 'z'; i++) {

		count = 0;

		for (int j = 0; j < text.length; j++) {

		if (i == text[j]){
			count++;
		}

		}

        if (count > maxNumberOfMeetings) {
			maxNumberOfMeetings = count;
			maxChar = i;
		}

		}

		return maxChar;

	}

	public static char[] deleteLetters(char text[],char theMostCommonLetter){
		char []newText=new char[text.length];
		int i=0;
		for (int j = 0; j < text.length; j++) {

		if (text[j]!= theMostCommonLetter) {
			newText[i]=text[j];
		} 		

		i++;
		}

		return newText;
		}


	public static void main(String[] args) {
		
		Scanner scanner=new Scanner(System.in);

		char text[]=scanner.nextLine().toCharArray();

		char firstMostCommonLetter=theMostCommonLetters(text);
		text=deleteLetters(text,firstMostCommonLetter);

		char secondMostCommonLetter=theMostCommonLetters(text);
		text=deleteLetters(text,secondMostCommonLetter);

		char thirdMostCommonLetter=theMostCommonLetters(text);

		System.out.println("firstMostCommonLetter= "+firstMostCommonLetter);
		System.out.println("secondMostCommonLetter= "+secondMostCommonLetter);
		System.out.println("thirdMostCommonLetter= "+thirdMostCommonLetter);
		
	}

}