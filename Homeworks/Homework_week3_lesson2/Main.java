import java.util.Random;

public class Main {
    public static void theMostCommonAge(Human humans[], int countHumans, int maxAgeOfHuman) {
        int count;
        int maxNumberOfMeetings = 0;
        int maxAge = 0;
        for (int i = 0; i < maxAgeOfHuman; i++) {
            count = 0;
            for (int j = 0; j < countHumans; j++) {

                if (i == humans[j].age) {
                    count++;
                }
            }

            if (count > maxNumberOfMeetings) {
                maxNumberOfMeetings = count;
                maxAge = i;
            }

        }

        System.out.println(maxAge+" -встретился "+maxNumberOfMeetings+" раз(а)");

    }

    public static void main(String[] args) {
        Random random = new Random();
        int countHumans = 100;
        int maxAgeOfHuman = 100;
        Human humans[] = new Human[countHumans];
        for (int i = 0; i < countHumans; i++) {
            humans[i] = new Human();
            humans[i].age = random.nextInt(maxAgeOfHuman);
            humans[i].name = ("human" + random.nextInt(1000)).toCharArray();
        }
        for (int i = 0; i < countHumans; i++) {
            System.out.println(humans[i].age + " " + humans[i].name);
        }
        System.out.println();
        theMostCommonAge(humans, countHumans, maxAgeOfHuman);
    }
}
